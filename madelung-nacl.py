#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#  Otto Emmersleben 
# "Über die Konvergenz der Reihen Epsteinscher Zetafunktionen. 
#  Erhard Schmidt zum, 75. Geburtstag."
#  https://doi.org/10.1002/mana.3210040140

#  Robert P. Grosso Jr., Justin T. Fermann, and William J. Vining
# "An In-Depth Look at the Madelung Constant for Cubic Crystal Systems"
#  https://doi.org/10.1021/ed078p1198

# https://github.com/kkirsche/python-MadelungConstant/blob/master/madelung.py

# https://scipython.com/book2/chapter-2-the-core-python-language-i/questions/problems/p24/evaluating-the-madelung-constant/

import numpy as np
import matplotlib.pyplot as plt
from itertools import product
import sys

def madelung_explain(n):
    #Langsame Variante
    madelung = 0
    for i in range(-n, n + 1):
        for j in range(-n, n + 1):
            for k in range(-n, n + 1):
                if i == 0 and j == 0 and k == 0:
                    continue
                madelung += (-1) ** (i + j + k) / (i**2 + j**2 + k**2)**0.5
    return madelung

def madelung(n):
    #Schnelle Variante
    n = n + 1
    indices = np.indices([n, n, n])
    i, j, k = indices[0], indices[1], indices[2]
    i[0, 0, 0] = 100  # to ensure that m[0, 0, 0] is not infinity
    m = (1 - 2 * ((i + j + k) % 2)) / np.sqrt(i**2 + j**2 + k**2)
    axes = m[0, 0, 1:].sum() + m[0, 1:, 0].sum() + m[1:, 0, 0].sum()
    faces = m[0, 1:, 1:].sum() + m[1:, 0, 1:].sum() + m[1:, 1:, 0].sum()
    off_axis = m[1:, 1:, 1:].sum() 
    madelung = (2 * axes) + (4 * faces) + (8 * off_axis) 
    
    mad_sing = []
    for i in range(1,n):
        axes = m[0, 0, 1:i].sum() + m[0, 1:i, 0].sum() + m[1:i, 0, 0].sum()
        faces = m[0, 1:i, 1:i].sum() + m[1:i, 0, 1:i].sum() + m[1:i, 1:i, 0].sum()
        off_axis = m[1:i, 1:i, 1:i].sum()
        mad = 2 * axes + 4 * faces + 8 * off_axis
        mad_sing.append(mad)
    
    madelung = (mad_sing[-1] + madelung)/2
    
    return madelung, mad_sing


def madelung2(n):
    i_values = np.arange(1, n, 2)
    j_values = np.arange(1, n, 2)
    
    i, j = np.meshgrid(i_values, j_values)
    fac = np.where(i == j, 1, 1)
    
    M = np.sum(fac * np.cosh(0.5 * np.pi * np.hypot(i, j))**-2)
    
    return -12 * np.pi * M


# def find_all_solutions_for_sum_of_squares(n):
#     solutions = []
#     max_val = int(np.sqrt(n)) + 1
#     for x in range(-max_val, max_val):
#         for y in range(-max_val, max_val):
#             for z in range(-max_val, max_val):
#                 if x**2 + y**2 + z**2 == n:
#                     solutions.append((x, y, z))
#     return len(solutions)
    
def find_all_solutions_for_sum_of_squares(n):
    solutions = []
    max_val = int(np.sqrt(n)) + 1
    for x, y, z in product(range(-max_val, max_val), repeat=3):
        if x**2 + y**2 + z**2 == n:
            solutions.append((x, y, z))
    return len(solutions)

def madelung_wrong(n):
    madelung = 0
    mpn_w = []
    for i in range(1, n+1):
        numerator = find_all_solutions_for_sum_of_squares(i)
        denominator = np.sqrt(i)
        mpn_w.append(madelung + (-1)**(i-1) * numerator / denominator)
        if numerator:
            #print((-1)**(i-1)*numerator,denominator**2)
            madelung += (-1)**(i-1) * numerator / denominator
    return mpn_w, madelung

# Beispiel mit N = 100 (N => 10)
N = 100
if N < 10 or N > 300:
    print('Nur für 10 <= N <= 300.')
    sys.exit(1)

#Klassische Methode
mpn_w, result = madelung_wrong(N)
#Lösungen mit '0' (n = x**2+y**2+z**2 hatte keine Lösung durch None ersetzen)
mpn_w = [None if item == 0 else item for item in mpn_w]
print(f'Madelung-Konstante für NaCl mit N={N} (klassisch):', '{:.8f}'.format(result))

#Neue Methode
result, mad_sing = madelung(N)
print(f'Madelung-Konstante für NaCl mit N={N} (neu)      :', '{:.8f}'.format(result))

#Neue Methode 2
result = madelung2(N)
print(f'Madelung-Konstante für NaCl mit N={N} (neu 2)    :', '{:.8f}'.format(result))

#Literaturwerte
print(f'Madelung-Konstante für NaCl Literaturwert        :', '{:.8f}'.format(-1.74756459))

#Einzelwerte für neue Methode 2 für Plot
mad2_sing = [madelung2(n) for n in range (1, N+1, 1)]

#Grafische Darstellung
fig = plt.figure()
ax  = fig.add_subplot()
x = np.arange(1, N+1, 1)

ax.plot(x,mpn_w,    '.',label='klassisch', color ='steelblue')
ax.plot(x,mad_sing, '.',label='alternativ', color = 'green')
ax.plot(x,mad2_sing,'.',label='alternativ 2', color = 'purple')

ax.set_xlim(1, N+1)
ax.set_ylim(-15, 15)
ax.set_yticks(np.arange(-15,16,3))
ax.set_xticks(np.arange(0, N+1, int(N/10)))
ax.hlines(-1.747564594633, 0, N+1, color='red',label = '-1.7476459',linestyles='dashed')
ax.set_title('Berechnung der Madelung-Konstante für NaCl')
ax.set_ylabel('Madelung-Konstante')
ax.set_xlabel('Anzahl der Sphären bzw. N')
ax.legend()
plt.tight_layout()
plt.show()
